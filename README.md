Solution for task 9


Upon running the code, two tests of the ChaosArray will be executed. One on a ChaosArray containing    ints, and one on a ChaosArray containing strings. The tests include adding a series of elements to the arrays, then retrieving some of these elements. The custom exceptions will be caught as they occur, and a message will be printed when they are caught. Finally all remaining elements in the ChaosArray  will be printed with a foreach loop to demonstrate the use of IEnumerators.

The relevant code is found in /task9/Program.cs, /task9/ChaosArray.cs, /task9/ChaosArrayIndexEmptyExeption.cs, /task9/ChaosArrayIndexTakenExeption.cs, 
