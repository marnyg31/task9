﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Task9
{
    class ChaosArray<T> : System.Collections.IEnumerable
    {
        private static int numElements = 20;
        Random rnd = new Random();

        //initilize an array to hold elements of type T
        T[] baseArray = new T[numElements];
        //initialize an array to bools indicate whether or not an index in baseArray is free or not
        bool[] baseArrayPositionTaken = new bool[numElements];


        public void Add(T t)
        {
            int randomPos = rnd.Next(numElements);
            if (!baseArrayPositionTaken[randomPos])
            {
                //if index is empty, put element in baseArray at that index. 
                //Also mark that index to true in baseArrayPostionTaken to mark that this index is used
                baseArray[randomPos] = t;
                baseArrayPositionTaken[randomPos] = true;
            }
            else
            {
                throw new ChaosArrayIndexTakenExeption();
            }

        }
        public T Get()
        {
            int randomPos = rnd.Next(numElements);
            if (baseArrayPositionTaken[randomPos])
            {
                //if index is not empty, get te element at that index
                //Also mark that index to false in baseArrayPostionTaken to mark that this index now free
                T element = baseArray[randomPos];
                baseArrayPositionTaken[randomPos] = false;
                return element;
            }
            else
            {
                throw new ChaosArrayIndexEmptyExeption();
            }
        }

        public IEnumerator GetEnumerator()
        {
            bool baseArrayEmpty = false;
            while (!baseArrayEmpty)
            {
                //Check if ChaosArray has any elements
                baseArrayEmpty = Array.TrueForAll<bool>(baseArrayPositionTaken, b => !b);

                //If array still has elements, try to get element at a random position
                //Repeat until array is empty
                int rndPos=rnd.Next(numElements);
                if (baseArrayPositionTaken[rndPos])
                {
                    baseArrayPositionTaken[rndPos]=false;
                    yield return baseArray[rndPos];
                }
            }
        }
    }
}
