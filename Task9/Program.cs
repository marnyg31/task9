﻿using System;

namespace Task9
{
    class Program
    {

        static void Main(string[] args)
        {
            //Testing chaos array with ints
            Console.WriteLine("test one ------------------------------------------------\n");
            TestChaosArrayWithInts();

            //Testing chaos array with strings
            Console.WriteLine("\n\ntest two ------------------------------------------------\n");
            TestChaosArrayWithStrings();

        }

        private static void TestChaosArrayWithStrings()
        {
            ChaosArray<string> chaosArray = new ChaosArray<string>();
            //Trying to add elements to array
            AddElementsToChaosArray<string>(chaosArray, new string[] { "a","ab","abs","b","c","d","f"});
            //Trying to get elements to array
            GetElementsFromChaosArray(chaosArray, 5);
            //Print what is left in the array
            PrintChaosArray<string>(chaosArray);
        }

        private static void TestChaosArrayWithInts()
        {
            ChaosArray<int> chaosArray = new ChaosArray<int>();
            //Trying to add elements to array
            AddElementsToChaosArray<int>(chaosArray, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            //Trying to get elements to array
            GetElementsFromChaosArray(chaosArray, 5);
            //Print what is left in the array
            PrintChaosArray<int>(chaosArray);
        }

        private static void PrintChaosArray<T>(ChaosArray<T> chaosArray)
        {
            Console.WriteLine("\nPrinting all elements in chaosArray:");
            foreach (T a in chaosArray)
            {
                Console.Write(a+",");
            }
        }

        private static void GetElementsFromChaosArray<T>(ChaosArray<T> chaosArray, int numGets)
        {
            foreach (int i in new int[numGets])
            {
                try
                {
                    Console.WriteLine( "Get from chaosArray: "+ chaosArray.Get());
                }
                catch (ChaosArrayIndexEmptyExeption e)
                {
                    Console.WriteLine("Tried to get an empty index");
                }
            }
        }

        private static void AddElementsToChaosArray<T>(ChaosArray<T> chaosArray, T[] array)
        {
            foreach (T i in array)
            {
                try
                {
                    chaosArray.Add(i);
                }
                catch (ChaosArrayIndexTakenExeption e)
                {
                    Console.WriteLine("tried to add to already full index");
                }
            }
        }
    }
}
