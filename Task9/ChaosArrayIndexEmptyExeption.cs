﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class ChaosArrayIndexEmptyExeption : Exception
    {
        public ChaosArrayIndexEmptyExeption()
        {
            //default override
        }

        public ChaosArrayIndexEmptyExeption(string message)
            : base(message)
        {
            //default override
        }

        public ChaosArrayIndexEmptyExeption(string message, Exception inner)
            : base(message, inner)
        {
            //default override
        }
    }
}
