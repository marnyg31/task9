﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class ChaosArrayIndexTakenExeption: Exception
    {
        public ChaosArrayIndexTakenExeption()
        {
            //default override
        }

        public ChaosArrayIndexTakenExeption(string message)
            : base(message)
        {
            //default override
        }

        public ChaosArrayIndexTakenExeption(string message, Exception inner)
            : base(message, inner)
        {
            //default override
        }
    }
}
